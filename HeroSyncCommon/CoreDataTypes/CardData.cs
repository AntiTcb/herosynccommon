﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DarkRift;

namespace HeroSyncCommon
{
    public class CardData : IDarkRiftSerializable
    {
        public short strength;
        public short level;
        public short ID;
        public short gameID;
        public CardType type;
        public short heroID;
        public string englishName;
        public string englishDescription;
        public List<CardActionEnum> availableActions;

        public void Deserialize(DeserializeEvent e)
        {
            strength = e.Reader.ReadInt16();
            ID = e.Reader.ReadInt16();
            level = e.Reader.ReadInt16();
            type = (CardType) e.Reader.ReadInt32();
            heroID = e.Reader.ReadInt16();
            gameID = e.Reader.ReadInt16();
            englishName = e.Reader.ReadString();
            englishDescription = e.Reader.ReadString();
            int actionCount = e.Reader.ReadInt32();
            availableActions = new List<CardActionEnum>();
            for (int i = 0; i < actionCount; i++)
            {
                availableActions.Add((CardActionEnum)e.Reader.ReadInt32());
            }
        }

        public void Serialize(SerializeEvent e)
        {

            e.Writer.Write(strength);
            e.Writer.Write(ID);
            e.Writer.Write(level);
            e.Writer.Write((int)type);
            e.Writer.Write(heroID);
            e.Writer.Write(gameID);
            if (englishName == null)
            {
                Console.Error.WriteLine("Serializer needs card to have english name. Giving card name 'temp'");
                englishName = "temp";
            }

            e.Writer.Write(englishName);

            if (englishDescription == null)
            {
                Console.Error.WriteLine("Serializer needs card to have english description. Giving card desc 'temp'");
                englishDescription = "temp";
            }


            e.Writer.Write(englishDescription);

            if (availableActions == null)
            {
                availableActions = new List<CardActionEnum>();
            }

            e.Writer.Write(availableActions.Count);
            foreach (CardActionEnum action in availableActions)
            {
                e.Writer.Write((int)action);
            }
        }

        public override bool Equals(object obj)
        {
            if (this.GetType() == obj.GetType())
            {
                CardData otherCard = (CardData) obj;
                return this.gameID == otherCard.gameID;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return gameID;
        }
    }

    public enum CardType
    {
        Follower,
        Drive,
        Counter,
        Token
    }

    // TODO: Replace using Card Action Enums with Action objects. 
    public enum CardActionEnum
    {
        SummonFollower,
        Play,
        Discard,
        SetCounter,
        ActivateCounter,
        Attack,
        ActivateAbility
    }

    public enum CardLocation
    {
        Deck,
        Hand,
        Field,
        FallenArea,
        Erased
    }
}
