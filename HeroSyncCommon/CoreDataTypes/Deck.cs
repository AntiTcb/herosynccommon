﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DarkRift;

namespace HeroSyncCommon
{
    public class Deck : IDarkRiftSerializable
    {
        public List<CardData> cards;
        public int id;

        public Deck()
        {
            cards = new List<CardData>();
        }

        public void Deserialize(DeserializeEvent e)
        {
            id = e.Reader.ReadInt32();
            cards = new List<CardData>();
            int cardCount = e.Reader.ReadInt32();
            for (int i = 0; i < cardCount; i++)
            {
                cards.Add(e.Reader.ReadSerializable<CardData>());
            }

        }

        public void Serialize(SerializeEvent e)
        {
            e.Writer.Write(id);
            e.Writer.Write(cards.Count);
            foreach (CardData card in cards)
            {
                e.Writer.Write(card);
            }
        }

        public void AddCard(CardData card)
        {
            cards.Add(card);
        }
    }
}
