﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeroSyncCommon
{
    public static class NetworkStateTags
    {
        public enum ServerMessageTags
        {
            ADD_BLUE_TO_ROOM = 0,
            ADD_ORANGE_TO_ROOM = 1,
            MOVE_CARD_TO_FIELD = 2,
            MOVE_CARD_TO_FALLEN = 3,
            CHANGE_BLUE_INFLUENCE = 4,
            CHANGE_ORANGE_INFLUENCE = 5,
            SET_COUNTER = 6,
            OPTION_SET = 7,
            UPDATE_PLAYER_DATA = 8,
            UPDATE_CARD_DATA = 9,
            SET_CURRENT_PLAYER = 10,
            ERASE_CARD = 11,
            MOVE_CARD_TO_DECK = 12,
            MOVE_CARD_GENERIC = 13,
            GAME_OVER = 14,
            LIST_ROOMS = 101,
            ACCEPT_LOGIN = 102,
            UPDATE_ROOMS = 103,
            GAME_START = 104
        }

        public enum ClientMessageTags
        {
            START_SINGLE_PLAYER_DEBUG = 0,
            SUMMON_MONSTER = 1,
            PLAY_DRIVE = 2,
            ATTACK = 3,
            SET_COUNTER = 4,
            ATTACK_TARGET_REQUEST = 5,
            END_TURN = 6,
            SEND_LOGIN_INFO = 100,
            SEND_ROOM_CHANGE = 101,
            JOIN_ROOM = 102,
            LOGOUT = 103,
            CREATE_ROOM = 104,
            DELETE_ROOM = 105,
            START_GAME = 106,
            LEAVE_ROOM = 107,
            SET_READY_TO_START = 108,
            GAME_ROOM_ENTERED = 109,
            DEBUG_OPP_SUMMON_MONSTER = 1001,
            DEBUG_FORCE_OPP_END_TURN = 1002,
        }
    }
}
