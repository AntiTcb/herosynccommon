﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DarkRift.Server;
using HeroSyncCommon.GameLogic;

namespace HeroSyncCommon
{
    public class PlayerField
    {
        public Dictionary<Zone, CardData> cardsInPlay;
        public List<CardData> orangeFallenArea;
        public List<CardData> blueFallenArea;
        public Deck blueDeck;
        public Deck orangeDeck;
        public List<CardData> orangeBanishedArea;
        public List<CardData> blueBanishedArea;

        public PlayerField(Deck orangeDeck, Deck blueDeck)
        {
            cardsInPlay = new Dictionary<Zone, CardData>();
            orangeFallenArea = new List<CardData>();
            blueFallenArea = new List<CardData>();
            this.blueDeck = blueDeck;
            this.orangeDeck = orangeDeck;
        }

        public enum Zone
        {
            BlueFollowerSlot1 = 1,
            BlueFollowerSlot2 = 2,
            BlueFollowerSlot3 = 3 ,
            BlueCounterSlot1 = 4,
            BlueCounterSlot2 = 5,
            BlueFallenArea = 6,
            BlueDeckArea = 7,
            BlueErasedArea = 8,
            BlueHand = 17,
            OrangeFollowerSlot1 = 9,
            OrangeFollowerSlot2 = 10,
            OrangeFollowerSlot3 = 11,
            OrangeCounterSlot1 = 12,
            OrangeCounterSlot2 = 13,
            OrangeFallenArea = 14,
            OrangeDeckArea = 15,
            OrangeErasedArea = 16,
            OrangeHand = 25,
            Null = 0
        }
    }
}
