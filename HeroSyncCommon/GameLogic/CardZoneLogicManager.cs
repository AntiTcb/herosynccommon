﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using static HeroSyncCommon.PlayerField;

namespace HeroSyncCommon.GameLogic
{
    public static class CardZoneLogicManager
    {
        /**
         * This should always be called after add cards to zone.
         */
        public static void AddCardsToZone(this PlayerField field, CardData card, PlayerField.Zone zone)
        {
            if (ZoneConverter.ToLocation(zone) == CardLocation.Field)
            {
                field.cardsInPlay[zone] = card;
            }
            else if (ZoneConverter.ToLocation(zone) == CardLocation.Deck)
            {
                if (zone == Zone.BlueDeckArea)
                {
                    field.blueDeck.AddCard(card);
                }
                else if (zone == Zone.OrangeDeckArea)
                {
                    field.orangeDeck.AddCard(card);
                }
            }
            else if (ZoneConverter.ToLocation(zone) == CardLocation.FallenArea)
            {
                if (zone == Zone.BlueFallenArea)
                {
                    field.blueFallenArea.Add(card);
                }
                else if (zone == Zone.OrangeFallenArea)
                {
                    field.orangeFallenArea.Add(card);
                }
            }
            else if (ZoneConverter.ToLocation(zone) == CardLocation.Hand)
            {
                Console.Error.Write("WARNING: You are using CardZoneLogicManager in HeroSynCommon to move cards to a hand. That should never really happen.");
            }

        }

        public static bool CanAddCardsToZone(this PlayerField field, CardData card, PlayerField.Zone zone)
        {
            bool placedCard = false;

            placedCard = CheckFallenArea(field, card, zone);
            if (!placedCard)
            {
                placedCard = CheckDeckArea(field, card, zone);
            }

            if (!placedCard)
            {
                placedCard = CheckFollowerAreas(field, card, zone);
            }

            if (!placedCard)
            {
                placedCard = CheckCounterAreas(field, card, zone);
            }

            if (!placedCard)
            {
                Console.Error.WriteLine("Tried to add a card to a field, could not find a place for it.");
            }

            return placedCard;
        }

        private static bool CheckCounterAreas(PlayerField field, CardData card, PlayerField.Zone zone)
        {
            if (!(zone == Zone.BlueCounterSlot1 || zone == Zone.BlueCounterSlot2 ||
                  zone == Zone.OrangeCounterSlot1 || zone == Zone.OrangeCounterSlot2))
            {
                return false;
            }

            if (card.type != CardType.Counter)
            {
                return false;
            }

            if (field.cardsInPlay.ContainsKey(zone))
            {
                return false;
            }

            return true;
        }

        private static bool CheckFollowerAreas(PlayerField field, CardData card, PlayerField.Zone zone)
        {
            if (!(zone == Zone.BlueFollowerSlot1 || zone == Zone.BlueFollowerSlot2 ||
                  zone == Zone.BlueFollowerSlot3 || zone == Zone.OrangeFollowerSlot1 ||
                  zone == Zone.OrangeFollowerSlot2 || zone == Zone.OrangeFollowerSlot3))
            {
                return false;
            }

            if (card.type != CardType.Follower && card.type != CardType.Token)
            {
                string errorMessage = String.Format("Tried to add {0} to a follower-specific zone.", card.englishName);
                Console.Error.WriteLine(errorMessage);
                return false;
            }
            else if (field.cardsInPlay.ContainsKey(zone))
            {
                return false;
            }
            // field.cardsInPlay.Add(zone, card);
            return true;
        }

        private static bool CheckDeckArea(PlayerField field, CardData card, PlayerField.Zone zone)
        {
            if (zone == Zone.BlueDeckArea)
            {
                // field.blueDeck.AddCard(card);
                return true;
            }
            else if (zone == Zone.OrangeDeckArea)
            {
                // field.orangeDeck.AddCard(card);
                return true;
            }

            return false;
        }

        private static bool CheckFallenArea(PlayerField field, CardData card, PlayerField.Zone zone)
        {
            if (zone == Zone.BlueFallenArea)
            {
                // field.blueFallenArea.Add(card);
                return true;
            }
            else if (zone == Zone.OrangeFallenArea)
            {
                // field.orangeFallenArea.Add(card);
                return true;
            }

            return false;
        }
    }
}