﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DarkRift;

namespace HeroSyncCommon.GameManagement
{
    public class RoomInfo : IDarkRiftSerializable
    {
        public int ID;
        public string name;
        public Account bluePlayer;
        public Account orangePlayer;

        public bool blueReady;
        public bool orangeReady;

        public RoomState currentState;

        public void Deserialize(DeserializeEvent e)
        {
            DarkRiftReader r = e.Reader;
            ID = r.ReadInt32();
            name = r.ReadString();
            bool hasBluePlayer = r.ReadBoolean();
            if (hasBluePlayer)
            {
                bluePlayer = r.ReadSerializable<Account>();
            }

            bool hasOrangePlayer = r.ReadBoolean();
            if (hasOrangePlayer)
            {
                orangePlayer = r.ReadSerializable<Account>();
            }

            currentState = (RoomState) r.ReadInt32();
            blueReady = r.ReadBoolean();
            orangeReady = r.ReadBoolean();

        }

        public void Serialize(SerializeEvent e)
        {
            DarkRiftWriter w = e.Writer;
            w.Write(ID);
            w.Write(name);
            bool hasBluePlayer = bluePlayer != null;
            w.Write(hasBluePlayer);
            if (hasBluePlayer)
            {
                w.Write(bluePlayer);
            }
            bool hasOrangePlayer = orangePlayer != null;
            w.Write(hasOrangePlayer);
            if (hasOrangePlayer)
            {
                w.Write(orangePlayer);
            }
            w.Write((int)currentState);
            w.Write(blueReady);
            w.Write(orangeReady);
        }

        public enum RoomState
        {
            EMPTY = 0,
            WAITING = 1,
            READY = 2,
            PLAYING = 3,
            GAME_ENDED = 4
        }
    }
}
