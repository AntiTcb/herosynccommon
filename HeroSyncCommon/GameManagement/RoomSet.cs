﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DarkRift;

namespace HeroSyncCommon.GameManagement
{
    public class RoomSet : IDarkRiftSerializable
    {
        public List<RoomInfo> rooms;

        public void Deserialize(DeserializeEvent e)
        {
            rooms = e.Reader.ReadSerializables<RoomInfo>().ToList();
        }

        public void Serialize(SerializeEvent e)
        {
            e.Writer.Write(rooms.ToArray());
        }
    }
}
