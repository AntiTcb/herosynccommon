﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DarkRift;

namespace HeroSyncCommon.NetworkActions
{
    public class AttackTargetRequest : IDarkRiftSerializable
    {
        public CardData attackingCard;

        public void Deserialize(DeserializeEvent e)
        {
            attackingCard = e.Reader.ReadSerializable<CardData>();
        }

        public void Serialize(SerializeEvent e)
        {
            if (e == null)
            {
                Console.Error.Write("Trying to serialize an empty attack target request.");
                return;
            }
            e.Writer.Write(attackingCard);
        }
    }
}
