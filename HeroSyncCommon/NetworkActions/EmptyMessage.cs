﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DarkRift;

/**
 * The empty message class is used when sending DarkRift the Network Tag would be sufficient, and no data is needed
 * to understand it. 
 */

namespace HeroSyncCommon.NetworkActions
{
    public class EmptyMessage : IDarkRiftSerializable
    {
        public string optionalMessage = "";
        public PlayerData sender;

        public void Deserialize(DeserializeEvent e)
        {
            optionalMessage = e.Reader.ReadString();
            bool attachedSender = e.Reader.ReadBoolean();
            if (attachedSender)
            {
                sender = e.Reader.ReadSerializable<PlayerData>();
            }
        }

        public void Serialize(SerializeEvent e)
        {
            e.Writer.Write(optionalMessage);
            bool attachedSender = sender != null;
            e.Writer.Write(attachedSender);
            if (attachedSender)
            {
                e.Writer.Write(sender);
            }
        }
    }
}
