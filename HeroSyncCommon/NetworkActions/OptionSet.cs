﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DarkRift;

namespace HeroSyncCommon.NetworkActions
{
    public class OptionSet : IDarkRiftSerializable
    {
        public int optionCount;
        public List<CardData> cardOptions;
        public List<CharacterData> characterOptions;
        public List<PlayerField.Zone> zoneOptions;
        public List<PlayerData> playerOptions;
        public List<String> textOptions;
        public List<(int, int)> numberRangeOptions;

        public bool canBeCancelled;

        public void Deserialize(DeserializeEvent e)
        {
            ListBookkeeping();
            DarkRiftReader r = e.Reader;
            optionCount = r.ReadInt32();
            canBeCancelled = r.ReadBoolean();
            Console.WriteLine("Deserializing {0} total options.", optionCount);
            for (int i = 0; i < optionCount; i++)
            {
                OptionType option = (OptionType) r.ReadInt32();
                switch (option)
                {
                    case OptionType.CARD:
                        cardOptions.Add(r.ReadSerializable<CardData>());
                        break;
                    case OptionType.HERO:
                        characterOptions.Add(r.ReadSerializable<CharacterData>());
                        break;
                    case OptionType.ZONE:
                        zoneOptions.Add((PlayerField.Zone) r.ReadInt32());
                        break;
                    case OptionType.PLAYER:
                        playerOptions.Add(r.ReadSerializable<PlayerData>());
                        break;
                    case OptionType.NUMBER_RANGE:
                        (int, int) pair = (r.ReadInt32(), r.ReadInt32());
                        numberRangeOptions.Add(pair);
                        break;
                    case OptionType.STRING:
                        textOptions.Add(r.ReadString());
                        break;
                    default:
                        Console.Error.WriteLine("Encountered unknown option type while deserializing option set.");
                        break;
                }
            }
        }



        public void Serialize(SerializeEvent e)
        {
            ListBookkeeping();

            DarkRiftWriter w = e.Writer;
            w.Write(optionCount);
            w.Write(canBeCancelled);
            Console.WriteLine("Serializing Option Set ");
            Console.WriteLine("Serializing {0} Card Options", cardOptions.Count);
            foreach (CardData card in cardOptions)
            {
                Console.WriteLine("Serializing Card ID: {0}, Name: {1}", card.gameID, card.englishName);
                w.Write((int) OptionType.CARD);
                w.Write(card);
            }

            Console.WriteLine("Serializing {0} Character Options", characterOptions.Count);
            foreach (CharacterData character in characterOptions)
            {
                Console.WriteLine("Serializing Char ID: {0}, Name: {1}", character.characterId, character.characterName);
                w.Write((int) OptionType.HERO);
                w.Write(character);
            }

            Console.WriteLine("Serializing {0} Zone Options", zoneOptions.Count);
            foreach (PlayerField.Zone z in zoneOptions)
            {
                w.Write((int) OptionType.ZONE);
                w.Write((int) z);
            }

            Console.WriteLine("Serializing {0} Player Options", playerOptions.Count);
            foreach (PlayerData player in playerOptions)
            {
                w.Write((int) OptionType.PLAYER);
                w.Write(player);
            }

            Console.WriteLine("Serializing {0} String Options", textOptions.Count);
            foreach (string opt in textOptions)
            {
                w.Write((int) OptionType.STRING);
                w.Write(opt);
            }

            Console.WriteLine("Serializing {0} number ranges", numberRangeOptions.Count);
            foreach ((int, int) rangePair in numberRangeOptions)
            {
                w.Write((int) OptionType.NUMBER_RANGE);
                w.Write(rangePair.Item1);
                w.Write(rangePair.Item2);
            }
        }


        public void ListBookkeeping()
        {
            if (cardOptions == null) cardOptions = new List<CardData>();
            if (characterOptions == null) characterOptions = new List<CharacterData>();
            if (zoneOptions == null) zoneOptions = new List<PlayerField.Zone>();
            if (playerOptions == null) playerOptions = new List<PlayerData>();
            if (textOptions == null) textOptions = new List<string>();
            if (numberRangeOptions == null) numberRangeOptions = new List<(int, int)>();

            optionCount = cardOptions.Count + characterOptions.Count + zoneOptions.Count + playerOptions.Count +
                          textOptions.Count + numberRangeOptions.Count;
        }

        public enum OptionType
        {
            CARD,
            HERO,
            ZONE,
            PLAYER,
            STRING,
            NUMBER_RANGE,
        }

    }
}
