﻿using DarkRift;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeroSyncCommon.NetworkActions
{
    public class PlayDrive : IDarkRiftSerializable
    {
        public CardData cardToPlay;
        public PlayerData.PlayerColor playerColor;

        public void Deserialize(DeserializeEvent e)
        {
            DarkRiftReader r = e.Reader;
            cardToPlay = r.ReadSerializable<CardData>();
            playerColor = (PlayerData.PlayerColor) r.ReadInt32();
        }

        public void Serialize(SerializeEvent e)
        {
            DarkRiftWriter w = e.Writer;
            w.Write(cardToPlay);
            w.Write((int)playerColor);
        }
    }
}
